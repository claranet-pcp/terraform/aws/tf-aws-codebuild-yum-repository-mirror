# tf-aws-codebuild-yum-repository-mirror

A module to enable you to easily setup mirroring of Yum repositories to S3 using Terraform.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| cloudwatch\_logs\_group\_name | The Cloudwatch logs group name to log to | string | n/a | yes |
| cloudwatch\_logs\_stream\_name | The Cloudwatch logs stream to log to. | string | n/a | yes |
| codebuild\_project\_name | The name of the mirror codebuild project | string | n/a | yes |
| repo\_mirror\_s3\_bucket\_name | The name of the S3 bucket to mirror the repository into. | string | n/a | yes |
| repo\_mirror\_s3\_prefix | The S3 prefix to mirror into in the S3 bucket specified. | string | n/a | yes |
| repo\_url\_to\_mirror | The URL of the yum repository to mirror. | string | n/a | yes |
| schedule\_expression | The schedule expression to use for triggering the mirror Codebuild project. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html | string | n/a | yes |
| buildspec\_content | Variable to override the default buildspec file for this module. | string | `"null"` | no |
| codebuild\_environment\_variables | Extra environment variables for the Codebuild | list | `[]` | no |
| codebuild\_iam\_role\_name\_prefix | The IAM role name prefix to use for the role created for the Codebuild project. | string | `"null"` | no |
| codebuild\_iam\_role\_policy\_name\_prefix | The IAM policy name prefix to use for the policy created for the Codebuild project role. | string | `"null"` | no |
| codebuild\_tags | Tags to assign to the Codebuild project | map | `{}` | no |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
