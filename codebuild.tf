locals {
  environment_variables = [
    {
      name  = "repo_url_to_mirror"
      value = var.repo_url_to_mirror
    },
    {
      name  = "repo_mirror_s3_bucket_name"
      value = var.repo_mirror_s3_bucket_name
    },
    {
      name  = "repo_mirror_s3_prefix"
      value = var.repo_mirror_s3_prefix
    },
  ]
}

resource "aws_codebuild_project" "repo_mirror" {
  name          = var.codebuild_project_name
  description   = "Mirrors the yum repository '${var.repo_url_to_mirror}' to the S3 bucket ${var.repo_mirror_s3_bucket_name} at ${var.repo_mirror_s3_prefix}."
  build_timeout = "480"
  service_role  = aws_iam_role.repo_mirror_codebuild_role.arn

  artifacts {
    type = "NO_ARTIFACTS"
  }

  source {
    type      = "NO_SOURCE"
    buildspec = var.buildspec_content != null ? var.buildspec_content : file("${path.module}/buildspec.yml")
  }

  environment {
    compute_type                = "BUILD_GENERAL1_MEDIUM"
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"

    dynamic "environment_variable" {
      for_each = concat(local.environment_variables, var.codebuild_environment_variables)

      content {
        name  = environment_variable.value["name"]
        value = environment_variable.value["value"]
      }
    }
  }

  logs_config {
    cloudwatch_logs {
      group_name  = var.cloudwatch_logs_group_name
      stream_name = var.cloudwatch_logs_stream_name
    }
  }

  tags = var.codebuild_tags
}