resource "aws_iam_role" "repo_mirror_codebuild_role" {
  name_prefix        = var.codebuild_iam_role_name_prefix != null ? var.codebuild_iam_role_name_prefix : "repo-mirror-codebuild-role"
  assume_role_policy = data.aws_iam_policy_document.codebuild_assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "repo_mirror_codebuild_role_policy" {
  role       = aws_iam_role.repo_mirror_codebuild_role.name
  policy_arn = aws_iam_policy.repo_mirror_codebuild_role_policy.arn
}

resource "aws_iam_policy" "repo_mirror_codebuild_role_policy" {
  name_prefix = var.codebuild_iam_role_policy_name_prefix != null ? var.codebuild_iam_role_policy_name_prefix : "repo-mirror-codebuild-role-policy"
  policy      = data.aws_iam_policy_document.repo_mirror_codebuild_role_policy.json
}

data "aws_iam_policy_document" "repo_mirror_codebuild_role_policy" {
  statement {
    sid = "S3BucketAccess"

    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:DeleteObject",
    ]

    resources = [
      "arn:aws:s3:::${var.repo_mirror_s3_bucket_name}",
      "arn:aws:s3:::${var.repo_mirror_s3_bucket_name}/*",
    ]
  }

  statement {
    sid = "CloudWatchLoggingAccess"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:${data.aws_partition.current.partition}:logs:*:${data.aws_caller_identity.current.account_id}:log-group:${var.cloudwatch_logs_group_name}:*",
      "arn:${data.aws_partition.current.partition}:logs:*:${data.aws_caller_identity.current.account_id}:log-group:${var.cloudwatch_logs_group_name}:log-stream:*",
      "arn:${data.aws_partition.current.partition}:logs:*:${data.aws_caller_identity.current.account_id}:log-group:${var.cloudwatch_logs_group_name}:log-stream:${var.cloudwatch_logs_stream_name}",
      "arn:${data.aws_partition.current.partition}:logs:*:${data.aws_caller_identity.current.account_id}:log-group:${var.cloudwatch_logs_group_name}:log-stream:${var.cloudwatch_logs_stream_name}/*",
    ]
  }
}
