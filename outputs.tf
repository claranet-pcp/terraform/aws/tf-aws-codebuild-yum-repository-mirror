output "codebuild_project_name" {
  value = aws_codebuild_project.repo_mirror.name
}

output "codebuild_project_service_role_arn" {
  value = aws_iam_role.repo_mirror_codebuild_role.arn
}

output "codebuild_project_service_role_name" {
  value = aws_iam_role.repo_mirror_codebuild_role.name
}