module "scheduled_execution" {
  source = "git::ssh://git@gitlab.com/claranet-pcp/terraform/aws/tf-aws-codebuild-schedule.git?ref=v0.0.1"
  cloudwatch_event_rule_name_prefix = "cb-sched-${aws_codebuild_project.repo_mirror.name}"
  codebuild_project_name = aws_codebuild_project.repo_mirror.name
  codebuild_project_arn = aws_codebuild_project.repo_mirror.arn
  codebuild_trigger_lambda_name = "cb-sched-${aws_codebuild_project.repo_mirror.name}"
  schedule_expression = var.schedule_expression
  schedule_enabled = var.schedule_expression != null ? true : false
}