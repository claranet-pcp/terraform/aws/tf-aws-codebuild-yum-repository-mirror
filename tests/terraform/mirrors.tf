module "codebuild_mirror_centos_updates_repo" {
  source                      = "../.."
  repo_url_to_mirror          = "rsync://mirror.bytemark.co.uk/centos/7/updates/x86_64/"
  repo_mirror_s3_bucket_name  = "${aws_s3_bucket.repo_mirror_bucket.bucket}"
  repo_mirror_s3_prefix       = "mirror/centos/updates"
  codebuild_project_name      = "mirror-centos-updates"
  cloudwatch_logs_group_name  = "repository-mirrors"
  cloudwatch_logs_stream_name = "centos-updates"
  schedule_expression         = "cron(5 17 * * ? *)"
}

module "codebuild_mirror_centos_sclo_repo" {
  source                      = "../.."
  repo_url_to_mirror          = "https://mirror.bytemark.co.uk/centos/7/sclo/x86_64/sclo/"
  repo_mirror_s3_bucket_name  = "${aws_s3_bucket.repo_mirror_bucket.bucket}"
  repo_mirror_s3_prefix       = "mirror/centos/sclo"
  codebuild_project_name      = "mirror-centos-sclo"
  cloudwatch_logs_group_name  = "repository-mirrors"
  cloudwatch_logs_stream_name = "centos-sclo"
  schedule_expression         = "cron(5 17 * * ? *)"
}