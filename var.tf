variable "codebuild_project_name" {
  description = "The name of the mirror codebuild project"
}

variable "repo_url_to_mirror" {
  description = "The URL of the yum repository to mirror."
  type        = string
}

variable "repo_mirror_s3_bucket_name" {
  description = "The name of the S3 bucket to mirror the repository into."
  type        = string
}

variable "repo_mirror_s3_prefix" {
  description = "The S3 prefix to mirror into in the S3 bucket specified."
  type        = string
}

variable "codebuild_environment_variables" {
  description = "Extra environment variables for the Codebuild"
  type        = list

  default = []
}

variable "buildspec_content" {
  description = "Variable to override the default buildspec file for this module."
  type        = string
  default     = null
}

variable "cloudwatch_logs_group_name" {
  description = "The Cloudwatch logs group name to log to"
  type        = string
}

variable "cloudwatch_logs_stream_name" {
  description = "The Cloudwatch logs stream to log to."
  type        = string
}

variable "codebuild_tags" {
  description = "Tags to assign to the Codebuild project"
  type        = map
  default     = {}
}

variable "codebuild_iam_role_name_prefix" {
  description = "The IAM role name prefix to use for the role created for the Codebuild project."
  type        = string
  default     = null
}

variable "codebuild_iam_role_policy_name_prefix" {
  description = "The IAM policy name prefix to use for the policy created for the Codebuild project role."
  type        = string
  default     = null
}

variable "schedule_expression" {
  description = "The schedule expression to use for triggering the mirror Codebuild project. See https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html"
  type = string
}
